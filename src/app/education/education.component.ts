import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-education',
  templateUrl: './education.component.html',
  styleUrls: ['./education.component.scss']
})
export class EducationComponent implements OnInit {
  timeline = [
    // {
    //   heading: "Infosys",
    //   duration: "Present",
    //   subtitle: "",
    //   content: "Upcoming Full stack develover at Infosys!",
    //   className1: "mar-left",
    //   className2: "prt_about_learnbox_right"
    // },
    {
      heading: "Mcs - Masters 2",
      duration: "2020",
      subtitle: "Université de Nantes",
      content: "Architectures Logicielles (ALMA)",
      className1: "mar-right",
      className2: "prt_about_learnbox_left"
    },
    {
      heading: "Master 1, Master 2",
      duration: "2019",
      subtitle: "Université de Souk-Ahras",
      content: "Génie Logiciel (GL) Major de promotion",
      className1: "mar-left",
      className2: "prt_about_learnbox_right"
    },
    {
      heading: "Licence SI",
      duration: "2017",
      subtitle: "Université de Souk-Ahras",
      content: "Systèmes Informatiques (SI) Major de promotion",
      className1: "mar-right",
      className2: "prt_about_learnbox_left"
    }
    ];

  constructor() { }

  ngOnInit(): void {
  }

}
