import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-skills',
  templateUrl: './skills.component.html',
  styleUrls: ['./skills.component.scss']
})
export class SkillsComponent implements OnInit {
  skillsSection = {
    title: "Ce que je fais 💁‍♂️",
    subTitle: "CRAZY FULL STACK DEVELOPER WHO WANTS TO EXPLORE EVERY TECH STACK",
    skills: [
      "⚡ DevOPS",
      "⚡ Développer des interfaces Front end / Utilisateur hautement interactives pour vos applications web et mobiles",
      "⚡ Création du backend de l'application dans Node, Express & Flask",
      "⚡ Développement des application de large échelle",
      "⚡ Intégration de services tiers et deploiment sur tels que AWS/ Cloud Oracle/ Heroku/ ",
      "⚡ Expérience de travail avec des grandes entreprises tels que Capgemini Alteca GIP-CPage."
    ]
  };
  constructor() { }

  ngOnInit(): void {
  }

}
