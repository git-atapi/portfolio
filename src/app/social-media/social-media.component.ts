import { Component, OnInit } from '@angular/core';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-social-media',
  templateUrl: './social-media.component.html',
  styleUrls: ['./social-media.component.scss']
})
export class SocialMediaComponent implements OnInit {
  socialMediaLinks = {

    github: "https://github.com/amiratkhaled",
    gitlab: "https://gitlab.com/git-atapi",
    linkedin: "https://www.linkedin.com/in/khaled-amirat/",
    email: "khaled@atapi.fr",
    facebookPage: "https://www.facebook.com/Atapi-194459621081719"
  };
  constructor() { }

  ngOnInit(): void {
  }

}
